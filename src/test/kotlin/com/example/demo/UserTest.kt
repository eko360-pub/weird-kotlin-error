package com.example.demo

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class UserTest {

    @Test
    fun serializationWorksInCode() {

        val expectedUser = User(id = "user1", age = 22, info = mapOf("more" to true, "1" to 1), name = "user1-name")
        val om = ObjectMapper().findAndRegisterModules()
        val json = """
            {
              "id": "user1",
              "age": 22,
              "name": "user1-name",
              "info": {
                "more": true,
                "1": 1
              }
            }
        """.trimIndent()

        val actualUser = om.readValue<User>(json)

        assertEquals(expectedUser, actualUser)
    }

}