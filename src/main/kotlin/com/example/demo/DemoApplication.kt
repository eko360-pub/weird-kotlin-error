package com.example.demo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.bind.annotation.*
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

@SpringBootApplication
class DemoApplication

fun main(args: Array<String>) {
    runApplication<DemoApplication>(*args)
}


@RestController
@RequestMapping("/demo")
class DemoController {

    @GetMapping
    fun getInfo() : Map<String,Map<String,Any?>> = mapOf(
            "env" to System.getenv(),
            "properties" to System.getProperties().asSequence().map { (k,v) -> k.toString() to v.toString() }.toMap()
    )

    @GetMapping("users/{id}")
    fun getUser(@PathVariable("id") id: String): User = User(id, name = "user-$id", age = 20, info = mapOf(
            "another-user" to User("22", "another-22", info = emptyMap(), age = 61),
            "1" to listOf(1),
            "2" to listOf(1, 2),
            "more" to mapOf("k" to 1, "v" to 2),
            "object" to object {
                val now = LocalDate.now()
                val id = UUID.randomUUID()
            }))

    @PostMapping("/users")
    fun postUser(user: User): Any {
        return object  {
            val user = user.copy(
                    id = UUID.randomUUID().toString(),
                    info = user.info + mapOf("dateCreated" to LocalDateTime.now()))
        }
    }
}

data class User(
        val id: String,
        val name: String,
        val age: Int,
        val info: Map<String,Any?>)


